# This is a header

Here we have some practice.

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands 

code block:

```sh
git clone www
cs ./node-task
npm i
```
Start cmd: `npm run start`

## The end

![alt](https://pixy.org/download/820926/)